<title>blurtRSS</title>

<img src="/blurt-rss.png" width="460px">
# BlurtRSS [https://rss.blurt.blog](https://rss.blurt.blog)

A simple tool for creating Atom/RSS feeds from Blurt accounts and categories.

Go to (almost) any Blurt page with a content stream, and replace 'blurt.blog' with 'rss.blurt.blog' in the URL. 

https://blurt.blog/trending/blurt -> https://***rss***.blurt.blurt/trending/blurt

## Usage Examples

### User Feed

Get posts from your feeds or others' as well.

> `rss.blurt.blog/@<username>/feed`

* [rss.blurt.blog/@saboin/feed](https://rss.blurt.blog/@saboin/feed)

### Posts By Author

Get posts from your favorite Blurter.

> `rss.blurt.blog/@<username>/blog`

* [rss.blurt.blog/@saboin/blog](https://rss.blurt.blog/@saboin/blog)

Filter posts to show only posts matching a tag

> `rss.blurt.blog/@username?tagFilter=tag`

* [rss.blurt.blog/@eastmael?tagFilter=blurt](https://rss.blurt.blog/@eastmael?tagFilter=blurt)

### Comments By Author

Get comments from your favorite blurter.

> `rss.blurt.blog/@<username>/comments`

* [rss.blurt.blog/@rycharde/comments](https://rss.blurt.blog/@rycharde/comments)

### Posts by Category

Get posts by category, you can get posts by new/hot/trending/promoted

> `https://rss.blurt.blog/new/<category>` OR `https://rss.blurt.blog/created/<category>`
> `https://rss.blurt.blog/hot/<category>`
> `https://rss.blurt.blog/trending/<category>`
> `https://rss.blurt.blog/promoted/<category>`

* [https://rss.blurt.blog/new/bitcoin](https://rss.blurt.blog/new/bitcoin)
* [https://rss.blurt.blog/hot/byt](https://rss.blurt.blog/hot/byt)
* [https://rss.blurt.blog/trending/blurt](https://rss.blurt.blog/trending/blurt)

### Posts Voted by a Specific User

Get a feed of links to posts recently voted by a user

> `https://rss.blurt.blog/@<username>/votes`

* [https://rss.blurt.blog/@blurt-booster/votes](https://rss.blurt.blog/@blurt-booster/votes)

<!-- Only include votes above a specific percentage weight, and link to non-default interace

> `https://rss.blurt.blog/@<username>/votes?minVotePct=<percentage>&interface=<interface-name>`

* [https://rss.blurt.blog/@megadrive/votes?minVotePct=100&interface=peakd](https://rss.blurt.blog/@megadrive/votes?minVotePct=100&interface=peakd) -->

## Supported Blurt Frontends 

Use with the query parameter 'interface'. See examples below.

* Blurt.blog (default) `https://rss.blurt.blog/@<username>`
<!-- * PeakD.com `https://hiverss.com/@<username>?interface=peakd`
* Ecency.com `https://hiverss.com/@<username>?interface=ecency`
* LeoFinance.io `https://hiverss.com/@<username>?interface=leofinance`
* Hivelist.org `https://hiverss.com/@<username>?interface=hivelist`
* Ctptalk.com `https://hiverss.com/@<username>?interface=ctptalk`
* Splintertalk.io `https://hiverss.com/@<username>?interface=splintertalk`
* Reggaejahm.com `https://hiverss.com/@<username>?interface=reggaejahm`
* Sportstalksocial.com `https://hiverss.com/@<username>?interface=sportstalk`
* Weedcash.network `https://hiverss.com/@<username>?interface=weedcash`
* Hivehustlers.io `https://hiverss.com/@<username>?interface=hivehustlers`
* Naturalmedicine.io `https://hiverss.com/@<username>?interface=naturalmedicine`
* Dunksocial.io `https://hiverss.com/@<username>?interface=dunksocial`
* WeAreAliveAnd.social `https://hiverss.com/@<username>?interface=wearealive`
* MusicForLife.io `https://hiverss.com/@<username>?interface=musicforlife`
* Beatzchain.com `https://hiverss.com/@<username>?interface=beatzchain`
* Blocktunes.social `https://hiverss.com/@<username>?interface=blocktunes` -->

## Contributors

* Modified By : [@tekraze](https://blurt.blog/@tekraze)
<!-- * Creator of SteemRSS: [@philipkoon](https://hive.blog/@philipkoon)
* SteemRSS Contributor: [@doriitamar](https://hive.blog/@doriitamar)
* HiveRSS Maintainer: [@torrey.blog](https://hive.blog/@torrey.blog)
* HiveRSS Maintainer: [@hivetrending](https://hive.blog/@hivetrending) Twitter: [@HiveTrending](https://twitter.com/hivetrending) -->